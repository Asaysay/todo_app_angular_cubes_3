import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TasksService } from 'src/app/_services/tasks.service';
import { AuthService } from '../../_services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  inputTodo: string = "";
  task = {name:""};
  tasks: any = [];
  isTasksLoaded = false;
  isNoTasks = false;

  // axe d'amélioration : rendre id dynamique en le récupérant à l'authentification
  id_user = "3";

  constructor(
    private authService: AuthService,
    private tasksService: TasksService,
    private routes: Router
  ) { }

  ngOnInit() {
    this.tasks = [];
    this.readTasks();
  }

  readTasks(){
    this.tasksService.readTasks()
      .subscribe
      ((tasks: any) => {
        this.tasks = [];
        this.isNoTasks = false;
        console.log(tasks);
        for(let i = 0; i < tasks.body.length; i++){
          this.tasks.push(tasks.body[i]);
        }
        if(this.tasks[0].id_task == null){
          this.isNoTasks = true;
        }
        console.log(this.tasks);
        console.log(this.isNoTasks);
        this.isTasksLoaded = true;
      })
  }

  delete(id: any){
    console.log(id);

    this.tasksService.deleteTask(id) 
      .subscribe(() => {
        this.tasks = this.tasks.filter(() => this.tasks.id_task !== this.tasks.id_task);
        this.isTasksLoaded = false;      
        this.readTasks();
        // let url = "/dashboard";
        // this.routes.navigate([url]);
    });;
  

  }

  addTodo () {
    let newTask = {"task_name" : `${this.inputTodo}`, "id_user" : `${this.id_user}`};
  
    // console.log(newTask);
    this.tasksService.createTask(newTask).subscribe(() => {
      this.readTasks;
    });
    this.readTasks();
  }


  logout(){
    this.authService.logout();
  }

}