import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const URL = "http://localhost:8080/api/controler/task/"

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  constructor(private httpClient: HttpClient) { }

  readTasks(): Observable<any> {
    return this.httpClient.get(`${URL}` + 'read.php');
  }

  createTask(task:any){    
    return this.httpClient.post<any>(`${URL}`+ 'create.php', task);
  }

  deleteTask(id:any){
    return this.httpClient.delete(`${URL}`+ 'delete.php/?id=' +id);
  }


}
